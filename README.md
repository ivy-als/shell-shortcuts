# shell-shortcuts

## Change default java
```
upate-alternatives --config java
```

## DOCKER

```
# Build Image
docker build .
docker build -t axonivy/engine .

# Run Container
docker run -d -p 3306:3306 e MYSQL_ROOT_PASSWORD=1234 mysql
docker run -d -p 5432:5432 -e POSTGRES_PASSWORD=1234 postgres
docker run -d -p 3307:3306 -e MYSQL_ROOT_PASSWORD=1234 mariadb
docker run -d -p 8080:8080 -p 50000:50000 -v C:\Users\als\Desktop\jenkins-data:/var/jenkins_home jenkins
docker run -d -p 8080:8080 -p 50000:50000-v C:\Users\als\jenkins:/var/jenkins_home myjenk
docker run -d -p 9200:9200 elasticsearch
docker run -d -p 8888:8080 -v /tmp/webapps/app.war:/usr/local/tomcat/webapps/app.war tomcat
docker run -d -p 8081:8081 zugprodocker:5000/axonivy/axonivy-engine
docker run -d -p 5601:5601 -e ELASTICSEARCH_URL=http://127.0.0.1:19200 docker.elastic.co/kibana/kibana:5.5.2

# Execute command in running container
docker exec postgres psql --version

# Open shell in container
docker exec -i -t postgres /bin/bash

# Stop all containers
docker stop $(docker ps -q)

# Remove all containers
docker rm $(docker ps -a -q)

# Remove all images
docker rmi $(docker images -q)

# Clean it up
docker stop $(docker ps -q) && docker rm $(docker ps -a -q) && docker rmi $(docker images -q)
```

## OSGi

```
# List all bundles
ss

# Start & Stop bundle (based on ID from command ss)
start 112
stop 112
```

## CURL

```
# Download file
curl -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.4.0.zip

# Do not show progress
curl -s http://127.0.0.1:9200
```

## SCP

```
scp your_username@remotehost.edu:foobar.txt /some/local/directory
scp -r user@your.server.example.com:/path/to/foo /home/user/Desktop/
scp -r foo your_username@remotehost.edu:/some/remote/directory/bar
```

## ivy workspace

```
# Build Designer
cd /c/dev/trunk/development/ch.ivyteam.ivy.build.maven/modules/product.designer
mvn clean install -Dmaven.test.skip=true
mvn clean install -Dmaven.test.skip=true -DSKIP_JRE_BUNDLING=true

# Execute Tests
mvn test -Dtest=ch.ivyteam.ivy.server.rest.Test*
mvn clean integration-test -Dtest=ch.ivyteam.ivy.server.processelements.TestWebService*
mvn test -Dtest=ch.ivyteam.ivy.server.internal.apps.portal.TestDeployPortalApplication
mvn integration-test -Dtest=ch.ivyteam.ivy.server.internal.apps.portal.TestDeployPortalApplication

# Start webpack casemap ui
cd /c/dev/trunk/webProjects/CaseMapUI && npm run dev
```

## Linux

```
# Show diskspace
df -h

# Mount Windows Share
sudo mount -t cifs -o domain=SORECO.WAN -o username=als //zugprofile/Sekretariat /media/zugprofile-sekretariat
```


## Elasticsearch

```
# List all indicies
http://127.0.0.1:19200/_cat/indices?v

# Index anzeigen
http://127.0.0.1:19202/ivy.businessdata-test.web.data/

# Einzelnes Dokument anzeigen
http://127.0.0.1:19202/ivy.businessdata-test.web.data/test.web.Data/3cd4c0453f2345dc9b3c782065182ccf
```

## Git

```
# Force pull
git fetch --all
git reset --hard origin/master

# Create local and remote branch
git checkout -b newbranch
git push -u origin newbranch

# Delete local and remote branch
git branch -D branch_name
git push origin --delete branch_name
```